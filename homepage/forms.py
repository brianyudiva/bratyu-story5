from django import forms
from django.forms import widgets
from .models import ScheduleModel

class CustomDateInput(widgets.TextInput):
    input_type = 'date'

class CustomTimeInput(widgets.TextInput):
    input_type = 'time'

class ScheduleForm(forms.ModelForm):
    class Meta:
        model = ScheduleModel
        fields = {'nama','kategori', 'tempat', 'date', 'time'}
        widgets = {
            'date' : CustomDateInput,
            'time' : CustomTimeInput
        }
