from django.db import models
from django.forms import widgets
# Create your models here.

class CustomDateINput(widgets.TextInput):
    input_type = 'date'

class CustomTimeInput(widgets.TextInput):
    input_type = 'time'

class ScheduleModel(models.Model):
    date = models.DateField()
    time = models.TimeField(max_length = 100)
    nama = models.CharField(max_length = 100)
    tempat = models.CharField(max_length = 100)
    kategori = models.CharField(max_length = 100, choices = [('akademis','Akademis'),('keluarga', 'Keluarga'),
    ('kepanitiaan', 'Kepanitiaan'),('lainnya', 'Lainnya')])