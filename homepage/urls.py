from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('blog/', views.blog, name='blog'),
    path('schedule/', views.schedule, name='schedule'),
    path('jadwal/', views.scheduleView.get, name='jadwal'),
    path('hapus/', views.schedule_delete, name='hapus'),
    path('deleteobj/<int:id>', views.delete_objek, name= 'hapuss'),
]